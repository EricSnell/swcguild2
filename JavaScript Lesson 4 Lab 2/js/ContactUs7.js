function checkForm() {
    
    var name = document.getElementById("nameInput").value;
    var email = document.getElementById("emailInput").value;
    var phone = document.getElementById("phoneInput").value;
    var inquirySelect = document.getElementById("dropdown").value;
    var addInfoBox = document.getElementById("textInput").value;
    var checkedBox = document.getElementsByName("check");
    
    if (name == "") {
        alert("Please enter your name");
        return false;
    }
    
    else if (email == "" && phone == "") {
        alert("Please provide your email address or a phone number");
        return false;
    }

    else if (inquirySelect == "Other" && addInfoBox == "") {
        alert("\"Additional Information\" needs to be filled");
        return false;
    }
       
    for (var i = 0; i < checkedBox.length; i++) {
        if (checkedBox[i].checked) {
            return true;
            break;
        } else {
            alert("Please check at least one day");
            return false;
            break;
        }
    }
}